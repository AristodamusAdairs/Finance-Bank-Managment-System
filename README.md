Finance Management is an important task for all banking, financial and investment sectors. There are several issues regarding security, time and money in the existing finance management system. In order to overcome these issues, this finance management system in VB.NET has been built to manage daily financial details and effectively organize day-to-day records in a single software application.
You can download the complete project source code, project report, documentation and other necessary project files of Finance Management System from the download links provided in this Repository . Below, I have briefly described the features and system requirements of the project along with the comparison of the existing system and the proposed system.
# About Finance Management System:
The following things can be done with this software application. You can find more about the features in the project report. Here, I have just listed out some basic features.
* handle loan details and perform loan management
* record savings of bank account holders.
* keep details of share holders and their accounts
* calculate interest rates, profit and loss details
* provide statistics reports for financial analysis
* less manpower/human resource
* cost and time effective
In this finance management system project, only admin module is implemented, and this performs all the main operations in the system. The basic functions incorporated in this module are adding, deleting, editing and updating the financial details of employees and members. This module is further divided into other modules and sub-functions as follows:
* Admin login
* Add/Delete/Update/View/Save Employee
* Add/Delete/Update/View/Save Member
* Transaction module
* Expenditure module
* Income module
* Contra module
* Daybook module
* Profit and Loss
* Summary module
# Existing System
In the existing system, most of the transactions are done manually, whereas in the proposed system all the banking transactions are computerized using the software financial management system. Listed below are the problems encountered in the existing system:
* Lack of security of data
* More man power
* Time consuming
* Consumes large volume of pare work
* Needs manual calculations
* No direct role for the higher officials
* Damage of machines due to lack of attention
# Proposed System
* Security of data
* Ensure data accuracy
* Proper control of the higher officials
* Reduce the damages of the machines
* Minimize manual data entry
* Minimum time needed for the various processing
* Greater efficiency
* Better service
* User friendliness and interactive
* Minimum time required
# System Specification:
#### System requirements
* Processor     :   X86 Compatible processor with 1.7 GHz Clock speed
*  RAM          :   512 MB or more
* Hard disk     :    20 GB or more
* Monitor       :    VGA/SVGA
* Keyboard      :    104 Keys
* Mouse         :    2 buttons/ 3 buttons
#### Software Requirements
Operating System      :  Windows 2000/XP
Front end                      : Visual Basic 6.0
Back end          :  MS Access
