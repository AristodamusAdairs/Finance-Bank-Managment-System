create table  heads
( hcode  char(2)  primary key,
   hdesc   varchar2(20),
   budget  number(7)
);

create table indents
(  iid  number(4) primary key,
   hcode char(2)  references heads(hcode),
   di        date,
   idesc   varchar2(30),
   qty       number(3),
   placedby varchar2(20),
   approved  char(1),
   da              date
);

create table qutations
(  qid   number(5)  primary key,
   iid    number(4)  references indents(iid),
   dq    date,
   supplier  varchar2(15),
   amount   number(6),
   accepted  char(1),
   ds             date
);

   
