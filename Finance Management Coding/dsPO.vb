﻿'------------------------------------------------------------------------------
' <autogenerated>
'     This code was generated by a tool.
'     Runtime Version: 1.0.3705.0
'
'     Changes to this file may cause incorrect behavior and will be lost if 
'     the code is regenerated.
' </autogenerated>
'------------------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports System
Imports System.Data
Imports System.Runtime.Serialization
Imports System.Xml


<Serializable(),  _
 System.ComponentModel.DesignerCategoryAttribute("code"),  _
 System.Diagnostics.DebuggerStepThrough(),  _
 System.ComponentModel.ToolboxItem(true)>  _
Public Class dsPO
    Inherits DataSet
    
    Private tableINDENTS As INDENTSDataTable
    
    Public Sub New()
        MyBase.New
        Me.InitClass
        Dim schemaChangedHandler As System.ComponentModel.CollectionChangeEventHandler = AddressOf Me.SchemaChanged
        AddHandler Me.Tables.CollectionChanged, schemaChangedHandler
        AddHandler Me.Relations.CollectionChanged, schemaChangedHandler
    End Sub
    
    Protected Sub New(ByVal info As SerializationInfo, ByVal context As StreamingContext)
        MyBase.New
        Dim strSchema As String = CType(info.GetValue("XmlSchema", GetType(System.String)),String)
        If (Not (strSchema) Is Nothing) Then
            Dim ds As DataSet = New DataSet
            ds.ReadXmlSchema(New XmlTextReader(New System.IO.StringReader(strSchema)))
            If (Not (ds.Tables("INDENTS")) Is Nothing) Then
                Me.Tables.Add(New INDENTSDataTable(ds.Tables("INDENTS")))
            End If
            Me.DataSetName = ds.DataSetName
            Me.Prefix = ds.Prefix
            Me.Namespace = ds.Namespace
            Me.Locale = ds.Locale
            Me.CaseSensitive = ds.CaseSensitive
            Me.EnforceConstraints = ds.EnforceConstraints
            Me.Merge(ds, false, System.Data.MissingSchemaAction.Add)
            Me.InitVars
        Else
            Me.InitClass
        End If
        Me.GetSerializationData(info, context)
        Dim schemaChangedHandler As System.ComponentModel.CollectionChangeEventHandler = AddressOf Me.SchemaChanged
        AddHandler Me.Tables.CollectionChanged, schemaChangedHandler
        AddHandler Me.Relations.CollectionChanged, schemaChangedHandler
    End Sub
    
    <System.ComponentModel.Browsable(false),  _
     System.ComponentModel.DesignerSerializationVisibilityAttribute(System.ComponentModel.DesignerSerializationVisibility.Content)>  _
    Public ReadOnly Property INDENTS As INDENTSDataTable
        Get
            Return Me.tableINDENTS
        End Get
    End Property
    
    Public Overrides Function Clone() As DataSet
        Dim cln As dsPO = CType(MyBase.Clone,dsPO)
        cln.InitVars
        Return cln
    End Function
    
    Protected Overrides Function ShouldSerializeTables() As Boolean
        Return false
    End Function
    
    Protected Overrides Function ShouldSerializeRelations() As Boolean
        Return false
    End Function
    
    Protected Overrides Sub ReadXmlSerializable(ByVal reader As XmlReader)
        Me.Reset
        Dim ds As DataSet = New DataSet
        ds.ReadXml(reader)
        If (Not (ds.Tables("INDENTS")) Is Nothing) Then
            Me.Tables.Add(New INDENTSDataTable(ds.Tables("INDENTS")))
        End If
        Me.DataSetName = ds.DataSetName
        Me.Prefix = ds.Prefix
        Me.Namespace = ds.Namespace
        Me.Locale = ds.Locale
        Me.CaseSensitive = ds.CaseSensitive
        Me.EnforceConstraints = ds.EnforceConstraints
        Me.Merge(ds, false, System.Data.MissingSchemaAction.Add)
        Me.InitVars
    End Sub
    
    Protected Overrides Function GetSchemaSerializable() As System.Xml.Schema.XmlSchema
        Dim stream As System.IO.MemoryStream = New System.IO.MemoryStream
        Me.WriteXmlSchema(New XmlTextWriter(stream, Nothing))
        stream.Position = 0
        Return System.Xml.Schema.XmlSchema.Read(New XmlTextReader(stream), Nothing)
    End Function
    
    Friend Sub InitVars()
        Me.tableINDENTS = CType(Me.Tables("INDENTS"),INDENTSDataTable)
        If (Not (Me.tableINDENTS) Is Nothing) Then
            Me.tableINDENTS.InitVars
        End If
    End Sub
    
    Private Sub InitClass()
        Me.DataSetName = "dsPO"
        Me.Prefix = ""
        Me.Namespace = "http://www.tempuri.org/dsPO.xsd"
        Me.Locale = New System.Globalization.CultureInfo("en-US")
        Me.CaseSensitive = false
        Me.EnforceConstraints = true
        Me.tableINDENTS = New INDENTSDataTable
        Me.Tables.Add(Me.tableINDENTS)
    End Sub
    
    Private Function ShouldSerializeINDENTS() As Boolean
        Return false
    End Function
    
    Private Sub SchemaChanged(ByVal sender As Object, ByVal e As System.ComponentModel.CollectionChangeEventArgs)
        If (e.Action = System.ComponentModel.CollectionChangeAction.Remove) Then
            Me.InitVars
        End If
    End Sub
    
    Public Delegate Sub INDENTSRowChangeEventHandler(ByVal sender As Object, ByVal e As INDENTSRowChangeEvent)
    
    <System.Diagnostics.DebuggerStepThrough()>  _
    Public Class INDENTSDataTable
        Inherits DataTable
        Implements System.Collections.IEnumerable
        
        Private columnIDESC As DataColumn
        
        Private columnQTY As DataColumn
        
        Private columnAMOUNT As DataColumn
        
        Private columnSUPPLIER As DataColumn
        
        Private columnIID As DataColumn
        
        Private columnQID As DataColumn
        
        Friend Sub New()
            MyBase.New("INDENTS")
            Me.InitClass
        End Sub
        
        Friend Sub New(ByVal table As DataTable)
            MyBase.New(table.TableName)
            If (table.CaseSensitive <> table.DataSet.CaseSensitive) Then
                Me.CaseSensitive = table.CaseSensitive
            End If
            If (table.Locale.ToString <> table.DataSet.Locale.ToString) Then
                Me.Locale = table.Locale
            End If
            If (table.Namespace <> table.DataSet.Namespace) Then
                Me.Namespace = table.Namespace
            End If
            Me.Prefix = table.Prefix
            Me.MinimumCapacity = table.MinimumCapacity
            Me.DisplayExpression = table.DisplayExpression
        End Sub
        
        <System.ComponentModel.Browsable(false)>  _
        Public ReadOnly Property Count As Integer
            Get
                Return Me.Rows.Count
            End Get
        End Property
        
        Friend ReadOnly Property IDESCColumn As DataColumn
            Get
                Return Me.columnIDESC
            End Get
        End Property
        
        Friend ReadOnly Property QTYColumn As DataColumn
            Get
                Return Me.columnQTY
            End Get
        End Property
        
        Friend ReadOnly Property AMOUNTColumn As DataColumn
            Get
                Return Me.columnAMOUNT
            End Get
        End Property
        
        Friend ReadOnly Property SUPPLIERColumn As DataColumn
            Get
                Return Me.columnSUPPLIER
            End Get
        End Property
        
        Friend ReadOnly Property IIDColumn As DataColumn
            Get
                Return Me.columnIID
            End Get
        End Property
        
        Friend ReadOnly Property QIDColumn As DataColumn
            Get
                Return Me.columnQID
            End Get
        End Property
        
        Public Default ReadOnly Property Item(ByVal index As Integer) As INDENTSRow
            Get
                Return CType(Me.Rows(index),INDENTSRow)
            End Get
        End Property
        
        Public Event INDENTSRowChanged As INDENTSRowChangeEventHandler
        
        Public Event INDENTSRowChanging As INDENTSRowChangeEventHandler
        
        Public Event INDENTSRowDeleted As INDENTSRowChangeEventHandler
        
        Public Event INDENTSRowDeleting As INDENTSRowChangeEventHandler
        
        Public Overloads Sub AddINDENTSRow(ByVal row As INDENTSRow)
            Me.Rows.Add(row)
        End Sub
        
        Public Overloads Function AddINDENTSRow(ByVal IDESC As String, ByVal QTY As Decimal, ByVal AMOUNT As Decimal, ByVal SUPPLIER As String, ByVal IID As Decimal, ByVal QID As Decimal) As INDENTSRow
            Dim rowINDENTSRow As INDENTSRow = CType(Me.NewRow,INDENTSRow)
            rowINDENTSRow.ItemArray = New Object() {IDESC, QTY, AMOUNT, SUPPLIER, IID, QID}
            Me.Rows.Add(rowINDENTSRow)
            Return rowINDENTSRow
        End Function
        
        Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
            Return Me.Rows.GetEnumerator
        End Function
        
        Public Overrides Function Clone() As DataTable
            Dim cln As INDENTSDataTable = CType(MyBase.Clone,INDENTSDataTable)
            cln.InitVars
            Return cln
        End Function
        
        Protected Overrides Function CreateInstance() As DataTable
            Return New INDENTSDataTable
        End Function
        
        Friend Sub InitVars()
            Me.columnIDESC = Me.Columns("IDESC")
            Me.columnQTY = Me.Columns("QTY")
            Me.columnAMOUNT = Me.Columns("AMOUNT")
            Me.columnSUPPLIER = Me.Columns("SUPPLIER")
            Me.columnIID = Me.Columns("IID")
            Me.columnQID = Me.Columns("QID")
        End Sub
        
        Private Sub InitClass()
            Me.columnIDESC = New DataColumn("IDESC", GetType(System.String), Nothing, System.Data.MappingType.Element)
            Me.Columns.Add(Me.columnIDESC)
            Me.columnQTY = New DataColumn("QTY", GetType(System.Decimal), Nothing, System.Data.MappingType.Element)
            Me.Columns.Add(Me.columnQTY)
            Me.columnAMOUNT = New DataColumn("AMOUNT", GetType(System.Decimal), Nothing, System.Data.MappingType.Element)
            Me.Columns.Add(Me.columnAMOUNT)
            Me.columnSUPPLIER = New DataColumn("SUPPLIER", GetType(System.String), Nothing, System.Data.MappingType.Element)
            Me.Columns.Add(Me.columnSUPPLIER)
            Me.columnIID = New DataColumn("IID", GetType(System.Decimal), Nothing, System.Data.MappingType.Element)
            Me.Columns.Add(Me.columnIID)
            Me.columnQID = New DataColumn("QID", GetType(System.Decimal), Nothing, System.Data.MappingType.Element)
            Me.Columns.Add(Me.columnQID)
            Me.columnIID.AllowDBNull = false
            Me.columnQID.AllowDBNull = false
        End Sub
        
        Public Function NewINDENTSRow() As INDENTSRow
            Return CType(Me.NewRow,INDENTSRow)
        End Function
        
        Protected Overrides Function NewRowFromBuilder(ByVal builder As DataRowBuilder) As DataRow
            Return New INDENTSRow(builder)
        End Function
        
        Protected Overrides Function GetRowType() As System.Type
            Return GetType(INDENTSRow)
        End Function
        
        Protected Overrides Sub OnRowChanged(ByVal e As DataRowChangeEventArgs)
            MyBase.OnRowChanged(e)
            If (Not (Me.INDENTSRowChangedEvent) Is Nothing) Then
                RaiseEvent INDENTSRowChanged(Me, New INDENTSRowChangeEvent(CType(e.Row,INDENTSRow), e.Action))
            End If
        End Sub
        
        Protected Overrides Sub OnRowChanging(ByVal e As DataRowChangeEventArgs)
            MyBase.OnRowChanging(e)
            If (Not (Me.INDENTSRowChangingEvent) Is Nothing) Then
                RaiseEvent INDENTSRowChanging(Me, New INDENTSRowChangeEvent(CType(e.Row,INDENTSRow), e.Action))
            End If
        End Sub
        
        Protected Overrides Sub OnRowDeleted(ByVal e As DataRowChangeEventArgs)
            MyBase.OnRowDeleted(e)
            If (Not (Me.INDENTSRowDeletedEvent) Is Nothing) Then
                RaiseEvent INDENTSRowDeleted(Me, New INDENTSRowChangeEvent(CType(e.Row,INDENTSRow), e.Action))
            End If
        End Sub
        
        Protected Overrides Sub OnRowDeleting(ByVal e As DataRowChangeEventArgs)
            MyBase.OnRowDeleting(e)
            If (Not (Me.INDENTSRowDeletingEvent) Is Nothing) Then
                RaiseEvent INDENTSRowDeleting(Me, New INDENTSRowChangeEvent(CType(e.Row,INDENTSRow), e.Action))
            End If
        End Sub
        
        Public Sub RemoveINDENTSRow(ByVal row As INDENTSRow)
            Me.Rows.Remove(row)
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThrough()>  _
    Public Class INDENTSRow
        Inherits DataRow
        
        Private tableINDENTS As INDENTSDataTable
        
        Friend Sub New(ByVal rb As DataRowBuilder)
            MyBase.New(rb)
            Me.tableINDENTS = CType(Me.Table,INDENTSDataTable)
        End Sub
        
        Public Property IDESC As String
            Get
                Try 
                    Return CType(Me(Me.tableINDENTS.IDESCColumn),String)
                Catch e As InvalidCastException
                    Throw New StrongTypingException("Cannot get value because it is DBNull.", e)
                End Try
            End Get
            Set
                Me(Me.tableINDENTS.IDESCColumn) = value
            End Set
        End Property
        
        Public Property QTY As Decimal
            Get
                Try 
                    Return CType(Me(Me.tableINDENTS.QTYColumn),Decimal)
                Catch e As InvalidCastException
                    Throw New StrongTypingException("Cannot get value because it is DBNull.", e)
                End Try
            End Get
            Set
                Me(Me.tableINDENTS.QTYColumn) = value
            End Set
        End Property
        
        Public Property AMOUNT As Decimal
            Get
                Try 
                    Return CType(Me(Me.tableINDENTS.AMOUNTColumn),Decimal)
                Catch e As InvalidCastException
                    Throw New StrongTypingException("Cannot get value because it is DBNull.", e)
                End Try
            End Get
            Set
                Me(Me.tableINDENTS.AMOUNTColumn) = value
            End Set
        End Property
        
        Public Property SUPPLIER As String
            Get
                Try 
                    Return CType(Me(Me.tableINDENTS.SUPPLIERColumn),String)
                Catch e As InvalidCastException
                    Throw New StrongTypingException("Cannot get value because it is DBNull.", e)
                End Try
            End Get
            Set
                Me(Me.tableINDENTS.SUPPLIERColumn) = value
            End Set
        End Property
        
        Public Property IID As Decimal
            Get
                Return CType(Me(Me.tableINDENTS.IIDColumn),Decimal)
            End Get
            Set
                Me(Me.tableINDENTS.IIDColumn) = value
            End Set
        End Property
        
        Public Property QID As Decimal
            Get
                Return CType(Me(Me.tableINDENTS.QIDColumn),Decimal)
            End Get
            Set
                Me(Me.tableINDENTS.QIDColumn) = value
            End Set
        End Property
        
        Public Function IsIDESCNull() As Boolean
            Return Me.IsNull(Me.tableINDENTS.IDESCColumn)
        End Function
        
        Public Sub SetIDESCNull()
            Me(Me.tableINDENTS.IDESCColumn) = System.Convert.DBNull
        End Sub
        
        Public Function IsQTYNull() As Boolean
            Return Me.IsNull(Me.tableINDENTS.QTYColumn)
        End Function
        
        Public Sub SetQTYNull()
            Me(Me.tableINDENTS.QTYColumn) = System.Convert.DBNull
        End Sub
        
        Public Function IsAMOUNTNull() As Boolean
            Return Me.IsNull(Me.tableINDENTS.AMOUNTColumn)
        End Function
        
        Public Sub SetAMOUNTNull()
            Me(Me.tableINDENTS.AMOUNTColumn) = System.Convert.DBNull
        End Sub
        
        Public Function IsSUPPLIERNull() As Boolean
            Return Me.IsNull(Me.tableINDENTS.SUPPLIERColumn)
        End Function
        
        Public Sub SetSUPPLIERNull()
            Me(Me.tableINDENTS.SUPPLIERColumn) = System.Convert.DBNull
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThrough()>  _
    Public Class INDENTSRowChangeEvent
        Inherits EventArgs
        
        Private eventRow As INDENTSRow
        
        Private eventAction As DataRowAction
        
        Public Sub New(ByVal row As INDENTSRow, ByVal action As DataRowAction)
            MyBase.New
            Me.eventRow = row
            Me.eventAction = action
        End Sub
        
        Public ReadOnly Property Row As INDENTSRow
            Get
                Return Me.eventRow
            End Get
        End Property
        
        Public ReadOnly Property Action As DataRowAction
            Get
                Return Me.eventAction
            End Get
        End Property
    End Class
End Class
