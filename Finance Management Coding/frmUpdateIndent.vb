Imports System.Data.OleDb
Public Class frmUpdateIndent
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents txtIID As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtDI As System.Windows.Forms.TextBox
    Friend WithEvents txtHcode As System.Windows.Forms.TextBox
    Friend WithEvents txtPlacedBy As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtQty As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtIDesc As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.txtIID = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtDI = New System.Windows.Forms.TextBox()
        Me.txtHcode = New System.Windows.Forms.TextBox()
        Me.txtPlacedBy = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtQty = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtIDesc = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtIID
        '
        Me.txtIID.Location = New System.Drawing.Point(296, 23)
        Me.txtIID.Name = "txtIID"
        Me.txtIID.Size = New System.Drawing.Size(88, 20)
        Me.txtIID.TabIndex = 42
        Me.txtIID.Text = ""
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.SystemColors.Control
        Me.Label6.Enabled = False
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(178, 26)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(85, 13)
        Me.Label6.TabIndex = 41
        Me.Label6.Text = "Indent Number "
        '
        'txtDI
        '
        Me.txtDI.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDI.Location = New System.Drawing.Point(437, 80)
        Me.txtDI.Name = "txtDI"
        Me.txtDI.ReadOnly = True
        Me.txtDI.Size = New System.Drawing.Size(88, 20)
        Me.txtDI.TabIndex = 39
        Me.txtDI.TabStop = False
        Me.txtDI.Text = ""
        '
        'txtHcode
        '
        Me.txtHcode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHcode.Location = New System.Drawing.Point(145, 80)
        Me.txtHcode.Name = "txtHcode"
        Me.txtHcode.ReadOnly = True
        Me.txtHcode.Size = New System.Drawing.Size(88, 20)
        Me.txtHcode.TabIndex = 38
        Me.txtHcode.TabStop = False
        Me.txtHcode.Text = ""
        '
        'txtPlacedBy
        '
        Me.txtPlacedBy.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPlacedBy.Location = New System.Drawing.Point(441, 176)
        Me.txtPlacedBy.Name = "txtPlacedBy"
        Me.txtPlacedBy.ReadOnly = True
        Me.txtPlacedBy.Size = New System.Drawing.Size(88, 20)
        Me.txtPlacedBy.TabIndex = 37
        Me.txtPlacedBy.TabStop = False
        Me.txtPlacedBy.Text = ""
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Silver
        Me.Label5.Enabled = False
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(305, 176)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(93, 13)
        Me.Label5.TabIndex = 36
        Me.Label5.Text = "Indent Placed By"
        '
        'txtQty
        '
        Me.txtQty.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtQty.Location = New System.Drawing.Point(145, 168)
        Me.txtQty.Name = "txtQty"
        Me.txtQty.Size = New System.Drawing.Size(88, 20)
        Me.txtQty.TabIndex = 35
        Me.txtQty.Text = ""
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Silver
        Me.Label4.Enabled = False
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(81, 168)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(48, 13)
        Me.Label4.TabIndex = 34
        Me.Label4.Text = "Quantity"
        '
        'txtIDesc
        '
        Me.txtIDesc.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIDesc.Location = New System.Drawing.Point(145, 128)
        Me.txtIDesc.Name = "txtIDesc"
        Me.txtIDesc.Size = New System.Drawing.Size(384, 20)
        Me.txtIDesc.TabIndex = 33
        Me.txtIDesc.Text = ""
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Silver
        Me.Label3.Enabled = False
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(81, 128)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(63, 13)
        Me.Label3.TabIndex = 32
        Me.Label3.Text = "Description"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Silver
        Me.Label1.Enabled = False
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(81, 84)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 13)
        Me.Label1.TabIndex = 31
        Me.Label1.Text = "Head"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Silver
        Me.Panel1.Controls.AddRange(New System.Windows.Forms.Control() {Me.Label2})
        Me.Panel1.Location = New System.Drawing.Point(53, 56)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(528, 176)
        Me.Panel1.TabIndex = 40
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Silver
        Me.Label2.Enabled = False
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(288, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(78, 13)
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "Date of Indent"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(59, 254)
        Me.Button1.Name = "Button1"
        Me.Button1.TabIndex = 43
        Me.Button1.Text = "&Update"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(260, 255)
        Me.Button2.Name = "Button2"
        Me.Button2.TabIndex = 44
        Me.Button2.Text = "&Delete"
        '
        'Button3
        '
        Me.Button3.CausesValidation = False
        Me.Button3.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Button3.Location = New System.Drawing.Point(447, 252)
        Me.Button3.Name = "Button3"
        Me.Button3.TabIndex = 45
        Me.Button3.Text = "&Cancel"
        '
        'frmUpdateIndent
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(603, 300)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.Button3, Me.Button2, Me.Button1, Me.txtIID, Me.Label6, Me.txtDI, Me.txtHcode, Me.txtPlacedBy, Me.Label5, Me.txtQty, Me.Label4, Me.txtIDesc, Me.Label3, Me.Label1, Me.Panel1})
        Me.Name = "frmUpdateIndent"
        Me.Text = "frmUpdateIndent"
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub txtIID_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtIID.Validating
        ' get details of indent
        Dim con As New OleDbConnection(constr)
        con.Open()
        Dim cmd As New OleDbCommand("select * from indents where  approved = 'n' and iid = " & txtIID.Text, con)
        Dim dr As OleDbDataReader

        dr = cmd.ExecuteReader

        If dr.Read() Then
            ' copy data to controls
            txtHcode.Text = dr.Item("hcode")
            txtIDesc.Text = dr.Item("idesc")
            txtDI.Text = dr.Item("di")
            txtQty.Text = dr.Item("qty")
            txtPlacedBy.Text = dr.Item("placedby")

        Else
            MsgBox("Sorry! Invalid Indent Number.Please try again!", , "Error")
            e.Cancel = True
        End If

        dr.Close()
        con.Close()

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim con As New OleDbConnection(constr)
        Dim cmd As New OleDbCommand()
        con.Open()
        cmd.Connection = con
        cmd.CommandText = "DeleteIndent"
        cmd.CommandType = CommandType.StoredProcedure

        Dim p As New OleDbParameter("p_iid", OleDbType.Integer)
        p.Value = txtIID.Text
        cmd.Parameters.Add(p)

        Try
            cmd.ExecuteNonQuery()
            MsgBox("Indent Deleted Successfully", , "Status")
            clearform()
        Catch ex As Exception
            MsgBox("Error : " & ex.Message, , "Error")
        Finally
            con.Close()
        End Try
    End Sub

    Public Sub ClearForm()
        Dim c As System.Windows.Forms.Control

        For Each c In Me.Controls
            If TypeOf c Is System.Windows.Forms.TextBox Then
                c.Text = ""
            End If
        Next

        txtIID.Focus()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim con As New OleDbConnection(constr)
        Dim cmd As New OleDbCommand()
        con.Open()
        cmd.Connection = con
        cmd.CommandText = "UpdateIndent"
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("p_iid", OleDbType.Integer).Value = txtIID.Text
        cmd.Parameters.Add("p_qty", OleDbType.Integer).Value = txtQty.Text
        cmd.Parameters.Add("p_des", OleDbType.VarChar).Value = txtIDesc.Text
        Try
            cmd.ExecuteNonQuery()
            MsgBox("Indent Updated Successfully", , "Status")
        Catch ex As Exception
            MsgBox("Error : " & ex.Message, , "Error")
        Finally
            con.Close()
        End Try
    End Sub
End Class
