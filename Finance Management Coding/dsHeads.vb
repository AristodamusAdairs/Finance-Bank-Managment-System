﻿'------------------------------------------------------------------------------
' <autogenerated>
'     This code was generated by a tool.
'     Runtime Version: 1.0.3705.0
'
'     Changes to this file may cause incorrect behavior and will be lost if 
'     the code is regenerated.
' </autogenerated>
'------------------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports System
Imports System.Data
Imports System.Runtime.Serialization
Imports System.Xml


<Serializable(),  _
 System.ComponentModel.DesignerCategoryAttribute("code"),  _
 System.Diagnostics.DebuggerStepThrough(),  _
 System.ComponentModel.ToolboxItem(true)>  _
Public Class dsHeads
    Inherits DataSet
    
    Private tableHEADS As HEADSDataTable
    
    Public Sub New()
        MyBase.New
        Me.InitClass
        Dim schemaChangedHandler As System.ComponentModel.CollectionChangeEventHandler = AddressOf Me.SchemaChanged
        AddHandler Me.Tables.CollectionChanged, schemaChangedHandler
        AddHandler Me.Relations.CollectionChanged, schemaChangedHandler
    End Sub
    
    Protected Sub New(ByVal info As SerializationInfo, ByVal context As StreamingContext)
        MyBase.New
        Dim strSchema As String = CType(info.GetValue("XmlSchema", GetType(System.String)),String)
        If (Not (strSchema) Is Nothing) Then
            Dim ds As DataSet = New DataSet
            ds.ReadXmlSchema(New XmlTextReader(New System.IO.StringReader(strSchema)))
            If (Not (ds.Tables("HEADS")) Is Nothing) Then
                Me.Tables.Add(New HEADSDataTable(ds.Tables("HEADS")))
            End If
            Me.DataSetName = ds.DataSetName
            Me.Prefix = ds.Prefix
            Me.Namespace = ds.Namespace
            Me.Locale = ds.Locale
            Me.CaseSensitive = ds.CaseSensitive
            Me.EnforceConstraints = ds.EnforceConstraints
            Me.Merge(ds, false, System.Data.MissingSchemaAction.Add)
            Me.InitVars
        Else
            Me.InitClass
        End If
        Me.GetSerializationData(info, context)
        Dim schemaChangedHandler As System.ComponentModel.CollectionChangeEventHandler = AddressOf Me.SchemaChanged
        AddHandler Me.Tables.CollectionChanged, schemaChangedHandler
        AddHandler Me.Relations.CollectionChanged, schemaChangedHandler
    End Sub
    
    <System.ComponentModel.Browsable(false),  _
     System.ComponentModel.DesignerSerializationVisibilityAttribute(System.ComponentModel.DesignerSerializationVisibility.Content)>  _
    Public ReadOnly Property HEADS As HEADSDataTable
        Get
            Return Me.tableHEADS
        End Get
    End Property
    
    Public Overrides Function Clone() As DataSet
        Dim cln As dsHeads = CType(MyBase.Clone,dsHeads)
        cln.InitVars
        Return cln
    End Function
    
    Protected Overrides Function ShouldSerializeTables() As Boolean
        Return false
    End Function
    
    Protected Overrides Function ShouldSerializeRelations() As Boolean
        Return false
    End Function
    
    Protected Overrides Sub ReadXmlSerializable(ByVal reader As XmlReader)
        Me.Reset
        Dim ds As DataSet = New DataSet
        ds.ReadXml(reader)
        If (Not (ds.Tables("HEADS")) Is Nothing) Then
            Me.Tables.Add(New HEADSDataTable(ds.Tables("HEADS")))
        End If
        Me.DataSetName = ds.DataSetName
        Me.Prefix = ds.Prefix
        Me.Namespace = ds.Namespace
        Me.Locale = ds.Locale
        Me.CaseSensitive = ds.CaseSensitive
        Me.EnforceConstraints = ds.EnforceConstraints
        Me.Merge(ds, false, System.Data.MissingSchemaAction.Add)
        Me.InitVars
    End Sub
    
    Protected Overrides Function GetSchemaSerializable() As System.Xml.Schema.XmlSchema
        Dim stream As System.IO.MemoryStream = New System.IO.MemoryStream
        Me.WriteXmlSchema(New XmlTextWriter(stream, Nothing))
        stream.Position = 0
        Return System.Xml.Schema.XmlSchema.Read(New XmlTextReader(stream), Nothing)
    End Function
    
    Friend Sub InitVars()
        Me.tableHEADS = CType(Me.Tables("HEADS"),HEADSDataTable)
        If (Not (Me.tableHEADS) Is Nothing) Then
            Me.tableHEADS.InitVars
        End If
    End Sub
    
    Private Sub InitClass()
        Me.DataSetName = "dsHeads"
        Me.Prefix = ""
        Me.Namespace = "http://www.tempuri.org/dsHeads.xsd"
        Me.Locale = New System.Globalization.CultureInfo("en-US")
        Me.CaseSensitive = false
        Me.EnforceConstraints = true
        Me.tableHEADS = New HEADSDataTable
        Me.Tables.Add(Me.tableHEADS)
    End Sub
    
    Private Function ShouldSerializeHEADS() As Boolean
        Return false
    End Function
    
    Private Sub SchemaChanged(ByVal sender As Object, ByVal e As System.ComponentModel.CollectionChangeEventArgs)
        If (e.Action = System.ComponentModel.CollectionChangeAction.Remove) Then
            Me.InitVars
        End If
    End Sub
    
    Public Delegate Sub HEADSRowChangeEventHandler(ByVal sender As Object, ByVal e As HEADSRowChangeEvent)
    
    <System.Diagnostics.DebuggerStepThrough()>  _
    Public Class HEADSDataTable
        Inherits DataTable
        Implements System.Collections.IEnumerable
        
        Private columnHCODE As DataColumn
        
        Private columnHDESC As DataColumn
        
        Private columnBUDGET As DataColumn
        
        Friend Sub New()
            MyBase.New("HEADS")
            Me.InitClass
        End Sub
        
        Friend Sub New(ByVal table As DataTable)
            MyBase.New(table.TableName)
            If (table.CaseSensitive <> table.DataSet.CaseSensitive) Then
                Me.CaseSensitive = table.CaseSensitive
            End If
            If (table.Locale.ToString <> table.DataSet.Locale.ToString) Then
                Me.Locale = table.Locale
            End If
            If (table.Namespace <> table.DataSet.Namespace) Then
                Me.Namespace = table.Namespace
            End If
            Me.Prefix = table.Prefix
            Me.MinimumCapacity = table.MinimumCapacity
            Me.DisplayExpression = table.DisplayExpression
        End Sub
        
        <System.ComponentModel.Browsable(false)>  _
        Public ReadOnly Property Count As Integer
            Get
                Return Me.Rows.Count
            End Get
        End Property
        
        Friend ReadOnly Property HCODEColumn As DataColumn
            Get
                Return Me.columnHCODE
            End Get
        End Property
        
        Friend ReadOnly Property HDESCColumn As DataColumn
            Get
                Return Me.columnHDESC
            End Get
        End Property
        
        Friend ReadOnly Property BUDGETColumn As DataColumn
            Get
                Return Me.columnBUDGET
            End Get
        End Property
        
        Public Default ReadOnly Property Item(ByVal index As Integer) As HEADSRow
            Get
                Return CType(Me.Rows(index),HEADSRow)
            End Get
        End Property
        
        Public Event HEADSRowChanged As HEADSRowChangeEventHandler
        
        Public Event HEADSRowChanging As HEADSRowChangeEventHandler
        
        Public Event HEADSRowDeleted As HEADSRowChangeEventHandler
        
        Public Event HEADSRowDeleting As HEADSRowChangeEventHandler
        
        Public Overloads Sub AddHEADSRow(ByVal row As HEADSRow)
            Me.Rows.Add(row)
        End Sub
        
        Public Overloads Function AddHEADSRow(ByVal HCODE As String, ByVal HDESC As String, ByVal BUDGET As Decimal) As HEADSRow
            Dim rowHEADSRow As HEADSRow = CType(Me.NewRow,HEADSRow)
            rowHEADSRow.ItemArray = New Object() {HCODE, HDESC, BUDGET}
            Me.Rows.Add(rowHEADSRow)
            Return rowHEADSRow
        End Function
        
        Public Function FindByHCODE(ByVal HCODE As String) As HEADSRow
            Return CType(Me.Rows.Find(New Object() {HCODE}),HEADSRow)
        End Function
        
        Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
            Return Me.Rows.GetEnumerator
        End Function
        
        Public Overrides Function Clone() As DataTable
            Dim cln As HEADSDataTable = CType(MyBase.Clone,HEADSDataTable)
            cln.InitVars
            Return cln
        End Function
        
        Protected Overrides Function CreateInstance() As DataTable
            Return New HEADSDataTable
        End Function
        
        Friend Sub InitVars()
            Me.columnHCODE = Me.Columns("HCODE")
            Me.columnHDESC = Me.Columns("HDESC")
            Me.columnBUDGET = Me.Columns("BUDGET")
        End Sub
        
        Private Sub InitClass()
            Me.columnHCODE = New DataColumn("HCODE", GetType(System.String), Nothing, System.Data.MappingType.Element)
            Me.Columns.Add(Me.columnHCODE)
            Me.columnHDESC = New DataColumn("HDESC", GetType(System.String), Nothing, System.Data.MappingType.Element)
            Me.Columns.Add(Me.columnHDESC)
            Me.columnBUDGET = New DataColumn("BUDGET", GetType(System.Decimal), Nothing, System.Data.MappingType.Element)
            Me.Columns.Add(Me.columnBUDGET)
            Me.Constraints.Add(New UniqueConstraint("Constraint1", New DataColumn() {Me.columnHCODE}, true))
            Me.columnHCODE.AllowDBNull = false
            Me.columnHCODE.Unique = true
        End Sub
        
        Public Function NewHEADSRow() As HEADSRow
            Return CType(Me.NewRow,HEADSRow)
        End Function
        
        Protected Overrides Function NewRowFromBuilder(ByVal builder As DataRowBuilder) As DataRow
            Return New HEADSRow(builder)
        End Function
        
        Protected Overrides Function GetRowType() As System.Type
            Return GetType(HEADSRow)
        End Function
        
        Protected Overrides Sub OnRowChanged(ByVal e As DataRowChangeEventArgs)
            MyBase.OnRowChanged(e)
            If (Not (Me.HEADSRowChangedEvent) Is Nothing) Then
                RaiseEvent HEADSRowChanged(Me, New HEADSRowChangeEvent(CType(e.Row,HEADSRow), e.Action))
            End If
        End Sub
        
        Protected Overrides Sub OnRowChanging(ByVal e As DataRowChangeEventArgs)
            MyBase.OnRowChanging(e)
            If (Not (Me.HEADSRowChangingEvent) Is Nothing) Then
                RaiseEvent HEADSRowChanging(Me, New HEADSRowChangeEvent(CType(e.Row,HEADSRow), e.Action))
            End If
        End Sub
        
        Protected Overrides Sub OnRowDeleted(ByVal e As DataRowChangeEventArgs)
            MyBase.OnRowDeleted(e)
            If (Not (Me.HEADSRowDeletedEvent) Is Nothing) Then
                RaiseEvent HEADSRowDeleted(Me, New HEADSRowChangeEvent(CType(e.Row,HEADSRow), e.Action))
            End If
        End Sub
        
        Protected Overrides Sub OnRowDeleting(ByVal e As DataRowChangeEventArgs)
            MyBase.OnRowDeleting(e)
            If (Not (Me.HEADSRowDeletingEvent) Is Nothing) Then
                RaiseEvent HEADSRowDeleting(Me, New HEADSRowChangeEvent(CType(e.Row,HEADSRow), e.Action))
            End If
        End Sub
        
        Public Sub RemoveHEADSRow(ByVal row As HEADSRow)
            Me.Rows.Remove(row)
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThrough()>  _
    Public Class HEADSRow
        Inherits DataRow
        
        Private tableHEADS As HEADSDataTable
        
        Friend Sub New(ByVal rb As DataRowBuilder)
            MyBase.New(rb)
            Me.tableHEADS = CType(Me.Table,HEADSDataTable)
        End Sub
        
        Public Property HCODE As String
            Get
                Return CType(Me(Me.tableHEADS.HCODEColumn),String)
            End Get
            Set
                Me(Me.tableHEADS.HCODEColumn) = value
            End Set
        End Property
        
        Public Property HDESC As String
            Get
                Try 
                    Return CType(Me(Me.tableHEADS.HDESCColumn),String)
                Catch e As InvalidCastException
                    Throw New StrongTypingException("Cannot get value because it is DBNull.", e)
                End Try
            End Get
            Set
                Me(Me.tableHEADS.HDESCColumn) = value
            End Set
        End Property
        
        Public Property BUDGET As Decimal
            Get
                Try 
                    Return CType(Me(Me.tableHEADS.BUDGETColumn),Decimal)
                Catch e As InvalidCastException
                    Throw New StrongTypingException("Cannot get value because it is DBNull.", e)
                End Try
            End Get
            Set
                Me(Me.tableHEADS.BUDGETColumn) = value
            End Set
        End Property
        
        Public Function IsHDESCNull() As Boolean
            Return Me.IsNull(Me.tableHEADS.HDESCColumn)
        End Function
        
        Public Sub SetHDESCNull()
            Me(Me.tableHEADS.HDESCColumn) = System.Convert.DBNull
        End Sub
        
        Public Function IsBUDGETNull() As Boolean
            Return Me.IsNull(Me.tableHEADS.BUDGETColumn)
        End Function
        
        Public Sub SetBUDGETNull()
            Me(Me.tableHEADS.BUDGETColumn) = System.Convert.DBNull
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThrough()>  _
    Public Class HEADSRowChangeEvent
        Inherits EventArgs
        
        Private eventRow As HEADSRow
        
        Private eventAction As DataRowAction
        
        Public Sub New(ByVal row As HEADSRow, ByVal action As DataRowAction)
            MyBase.New
            Me.eventRow = row
            Me.eventAction = action
        End Sub
        
        Public ReadOnly Property Row As HEADSRow
            Get
                Return Me.eventRow
            End Get
        End Property
        
        Public ReadOnly Property Action As DataRowAction
            Get
                Return Me.eventAction
            End Get
        End Property
    End Class
End Class
