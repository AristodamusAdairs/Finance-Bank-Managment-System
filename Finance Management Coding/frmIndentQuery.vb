Imports System.Data.OleDb
Public Class frmIndentQuery
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmbHead As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtIndentBy As System.Windows.Forms.TextBox
    Friend WithEvents txtDesc As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents dtpFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents btnQuery As System.Windows.Forms.Button
    Friend WithEvents DataGrid1 As System.Windows.Forms.DataGrid
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.cmbHead = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtIndentBy = New System.Windows.Forms.TextBox()
        Me.txtDesc = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.dtpFrom = New System.Windows.Forms.DateTimePicker()
        Me.dtpTo = New System.Windows.Forms.DateTimePicker()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnQuery = New System.Windows.Forms.Button()
        Me.DataGrid1 = New System.Windows.Forms.DataGrid()
        CType(Me.DataGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmbHead
        '
        Me.cmbHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbHead.Location = New System.Drawing.Point(138, 16)
        Me.cmbHead.Name = "cmbHead"
        Me.cmbHead.Size = New System.Drawing.Size(121, 21)
        Me.cmbHead.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(50, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Head"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(399, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(54, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Indent By"
        '
        'txtIndentBy
        '
        Me.txtIndentBy.Location = New System.Drawing.Point(466, 21)
        Me.txtIndentBy.Name = "txtIndentBy"
        Me.txtIndentBy.Size = New System.Drawing.Size(174, 20)
        Me.txtIndentBy.TabIndex = 5
        Me.txtIndentBy.Text = ""
        '
        'txtDesc
        '
        Me.txtDesc.Location = New System.Drawing.Point(134, 46)
        Me.txtDesc.Name = "txtDesc"
        Me.txtDesc.Size = New System.Drawing.Size(504, 20)
        Me.txtDesc.TabIndex = 7
        Me.txtDesc.Text = ""
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(50, 48)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(63, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Description"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(50, 78)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(31, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "From"
        '
        'dtpFrom
        '
        Me.dtpFrom.Checked = False
        Me.dtpFrom.CustomFormat = "dd-MMM-yy"
        Me.dtpFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpFrom.Location = New System.Drawing.Point(132, 74)
        Me.dtpFrom.Name = "dtpFrom"
        Me.dtpFrom.ShowCheckBox = True
        Me.dtpFrom.Size = New System.Drawing.Size(147, 20)
        Me.dtpFrom.TabIndex = 9
        '
        'dtpTo
        '
        Me.dtpTo.Checked = False
        Me.dtpTo.CustomFormat = "dd-MMM-yy"
        Me.dtpTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpTo.Location = New System.Drawing.Point(489, 75)
        Me.dtpTo.Name = "dtpTo"
        Me.dtpTo.ShowCheckBox = True
        Me.dtpTo.Size = New System.Drawing.Size(147, 20)
        Me.dtpTo.TabIndex = 11
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(454, 78)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(18, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "To"
        '
        'btnQuery
        '
        Me.btnQuery.Location = New System.Drawing.Point(54, 111)
        Me.btnQuery.Name = "btnQuery"
        Me.btnQuery.TabIndex = 12
        Me.btnQuery.Text = "&Query"
        '
        'DataGrid1
        '
        Me.DataGrid1.CaptionText = "Selected Indents"
        Me.DataGrid1.DataMember = ""
        Me.DataGrid1.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.DataGrid1.Location = New System.Drawing.Point(17, 151)
        Me.DataGrid1.Name = "DataGrid1"
        Me.DataGrid1.ReadOnly = True
        Me.DataGrid1.Size = New System.Drawing.Size(641, 137)
        Me.DataGrid1.TabIndex = 13
        '
        'frmIndentQuery
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(671, 302)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.DataGrid1, Me.btnQuery, Me.dtpTo, Me.Label5, Me.dtpFrom, Me.Label4, Me.txtDesc, Me.Label3, Me.txtIndentBy, Me.Label2, Me.cmbHead, Me.Label1})
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmIndentQuery"
        Me.Text = "Query Indents"
        CType(Me.DataGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmIndentQuery_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim con As New OleDbConnection(constr)
        Dim da As New OleDbDataAdapter("select hcode,hdesc from heads  order by hdesc", con)
        Dim ds As New DataSet()
        da.Fill(ds, "heads")
        Dim dr As DataRow
        dr = ds.Tables(0).NewRow
        dr.Item(0) = "ALL"
        dr.Item(1) = "-- All Heads --"
        ds.Tables(0).Rows.InsertAt(dr, 0)

        ' bind data
        cmbHead.DataSource = ds.Tables("heads")
        cmbHead.DisplayMember = "HDESC"   '  column name must be in uppercase
        cmbHead.ValueMember = "HCODE"

    End Sub

    Private Sub btnQuery_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnQuery.Click
        Dim cond As String = "  1 =1   "

        If cmbHead.SelectedIndex > 0 Then
            cond = cond & " and   hcode = '" & cmbHead.SelectedValue & "'"
        End If

        If txtIndentBy.Text.Length > 0 Then
            cond = cond & " and  placedby  like '%" & txtIndentBy.Text & "%'"
        End If

        If txtDesc.Text.Length > 0 Then
            cond = cond & " and  idesc  like '%" & txtDesc.Text & "%'"
        End If

        If dtpFrom.Checked Then
            cond = cond & "  and  di >= '" & dtpFrom.Text & "'"
        End If

        If dtpTo.Checked Then
            cond = cond & "  and  di <= '" & dtpTo.Text & "'"
        End If


        Dim con As New OleDbConnection(constr)
        Dim da As New OleDbDataAdapter("select * from indents where  " & cond, con)
        Dim ds As New DataSet()
        da.Fill(ds, "Indents")

        DataGrid1.DataSource = ds.Tables(0)


    End Sub
End Class
