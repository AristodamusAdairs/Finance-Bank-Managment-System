Imports System.Data.OleDb
Public Class frmNewIndent
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmbHead As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtIDesc As System.Windows.Forms.TextBox
    Friend WithEvents txtQty As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtPlacedBy As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents dtpDI As System.Windows.Forms.DateTimePicker
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmbHead = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dtpDI = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtIDesc = New System.Windows.Forms.TextBox()
        Me.txtQty = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtPlacedBy = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(16, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Head"
        '
        'cmbHead
        '
        Me.cmbHead.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbHead.Location = New System.Drawing.Point(96, 16)
        Me.cmbHead.Name = "cmbHead"
        Me.cmbHead.Size = New System.Drawing.Size(121, 21)
        Me.cmbHead.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(256, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(78, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Date of Indent"
        '
        'dtpDI
        '
        Me.dtpDI.CustomFormat = "dd-MMM-yy"
        Me.dtpDI.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpDI.Location = New System.Drawing.Point(344, 16)
        Me.dtpDI.Name = "dtpDI"
        Me.dtpDI.Size = New System.Drawing.Size(136, 20)
        Me.dtpDI.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(24, 56)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(63, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Description"
        '
        'txtIDesc
        '
        Me.txtIDesc.Location = New System.Drawing.Point(96, 56)
        Me.txtIDesc.Name = "txtIDesc"
        Me.txtIDesc.Size = New System.Drawing.Size(384, 20)
        Me.txtIDesc.TabIndex = 5
        Me.txtIDesc.Text = ""
        '
        'txtQty
        '
        Me.txtQty.Location = New System.Drawing.Point(96, 96)
        Me.txtQty.Name = "txtQty"
        Me.txtQty.Size = New System.Drawing.Size(88, 20)
        Me.txtQty.TabIndex = 7
        Me.txtQty.Text = ""
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(32, 96)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(48, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Quantity"
        '
        'txtPlacedBy
        '
        Me.txtPlacedBy.Location = New System.Drawing.Point(392, 104)
        Me.txtPlacedBy.Name = "txtPlacedBy"
        Me.txtPlacedBy.Size = New System.Drawing.Size(88, 20)
        Me.txtPlacedBy.TabIndex = 9
        Me.txtPlacedBy.Text = ""
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(256, 104)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(93, 13)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "Indent Placed By"
        '
        'btnAdd
        '
        Me.btnAdd.Location = New System.Drawing.Point(120, 144)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.TabIndex = 10
        Me.btnAdd.Text = "&Add"
        '
        'Button2
        '
        Me.Button2.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Button2.Location = New System.Drawing.Point(312, 144)
        Me.Button2.Name = "Button2"
        Me.Button2.TabIndex = 11
        Me.Button2.Text = "&Cancel"
        '
        'frmNewIndent
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(496, 188)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.Button2, Me.btnAdd, Me.txtPlacedBy, Me.Label5, Me.txtQty, Me.Label4, Me.txtIDesc, Me.Label3, Me.dtpDI, Me.Label2, Me.cmbHead, Me.Label1})
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmNewIndent"
        Me.Text = "New Indent"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmNewIndent_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim con As New OleDbConnection(constr)
        Dim da As New OleDbDataAdapter("select hcode,hdesc from heads  order by hdesc", con)
        Dim ds As New DataSet()
        da.Fill(ds, "heads")

        ' bind data
        cmbHead.DataSource = ds.Tables("heads")
        cmbHead.DisplayMember = "HDESC"   '  column name must be in uppercase
        cmbHead.ValueMember = "HCODE"

    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim con As New OleDbConnection(constr)
        con.Open()
        Dim cmdstr As String

        cmdstr = "insert into indents values( sq_indentno.nextval,'" _
        & cmbHead.SelectedValue & "','" & dtpDI.Text & "','" _
        & txtIDesc.Text & "'," & txtQty.Text & ",'" & txtPlacedBy.Text & "','n',null)"

        Dim cmd As New OleDbCommand(cmdstr, con)
        Try
            cmd.ExecuteNonQuery()
            MsgBox("New Indent Added To Database Successfully", , "Status")
        Catch ex As Exception
            MsgBox(ex.Message, , "Error")
        Finally
            con.Close()
        End Try

    End Sub
End Class
