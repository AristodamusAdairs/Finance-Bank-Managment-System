Imports System.Data.OleDb
Imports CrystalDecisions.CrystalReports.Engine
Public Class frmPOReport
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents CrystalReportViewer1 As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents OleDbDataAdapter1 As System.Data.OleDb.OleDbDataAdapter
    Friend WithEvents OleDbConnection1 As System.Data.OleDb.OleDbConnection
    Friend WithEvents DsPO1 As fm.dsPO
    Friend WithEvents cmbIndents As System.Windows.Forms.ComboBox
    Friend WithEvents OleDbSelectCommand1 As System.Data.OleDb.OleDbCommand
    Friend WithEvents btnPO As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmbIndents = New System.Windows.Forms.ComboBox()
        Me.CrystalReportViewer1 = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.OleDbDataAdapter1 = New System.Data.OleDb.OleDbDataAdapter()
        Me.OleDbConnection1 = New System.Data.OleDb.OleDbConnection()
        Me.DsPO1 = New fm.dsPO()
        Me.OleDbSelectCommand1 = New System.Data.OleDb.OleDbCommand()
        Me.btnPO = New System.Windows.Forms.Button()
        CType(Me.DsPO1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(102, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Select Indent"
        '
        'cmbIndents
        '
        Me.cmbIndents.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbIndents.Location = New System.Drawing.Point(187, 8)
        Me.cmbIndents.Name = "cmbIndents"
        Me.cmbIndents.Size = New System.Drawing.Size(210, 21)
        Me.cmbIndents.TabIndex = 1
        '
        'CrystalReportViewer1
        '
        Me.CrystalReportViewer1.ActiveViewIndex = -1
        Me.CrystalReportViewer1.DisplayGroupTree = False
        Me.CrystalReportViewer1.Location = New System.Drawing.Point(34, 52)
        Me.CrystalReportViewer1.Name = "CrystalReportViewer1"
        Me.CrystalReportViewer1.ReportSource = Nothing
        Me.CrystalReportViewer1.ShowGotoPageButton = False
        Me.CrystalReportViewer1.ShowGroupTreeButton = False
        Me.CrystalReportViewer1.Size = New System.Drawing.Size(647, 193)
        Me.CrystalReportViewer1.TabIndex = 2
        Me.CrystalReportViewer1.Visible = False
        '
        'OleDbDataAdapter1
        '
        Me.OleDbDataAdapter1.SelectCommand = Me.OleDbSelectCommand1
        Me.OleDbDataAdapter1.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "INDENTS", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("IDESC", "IDESC"), New System.Data.Common.DataColumnMapping("QTY", "QTY"), New System.Data.Common.DataColumnMapping("AMOUNT", "AMOUNT"), New System.Data.Common.DataColumnMapping("SUPPLIER", "SUPPLIER"), New System.Data.Common.DataColumnMapping("IID", "IID"), New System.Data.Common.DataColumnMapping("QID", "QID")})})
        '
        'OleDbConnection1
        '
        Me.OleDbConnection1.ConnectionString = "Provider=MSDAORA.1;Password=fm;User ID=fm"
        '
        'DsPO1
        '
        Me.DsPO1.DataSetName = "dsPO"
        Me.DsPO1.Locale = New System.Globalization.CultureInfo("en-US")
        Me.DsPO1.Namespace = "http://www.tempuri.org/dsPO.xsd"
        '
        'OleDbSelectCommand1
        '
        Me.OleDbSelectCommand1.CommandText = "SELECT I.IDESC, I.QTY, Q.AMOUNT, Q.SUPPLIER, I.IID, Q.QID FROM INDENTS I, QUOTATI" & _
        "ONS Q WHERE I.IID = Q.IID AND (I.IID = ?) AND (Q.ACCEPTED = 'y')"
        Me.OleDbSelectCommand1.Connection = Me.OleDbConnection1
        Me.OleDbSelectCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("IID", System.Data.OleDb.OleDbType.Decimal, 0, System.Data.ParameterDirection.Input, False, CType(4, Byte), CType(0, Byte), "IID", System.Data.DataRowVersion.Current, Nothing))
        '
        'btnPO
        '
        Me.btnPO.Location = New System.Drawing.Point(438, 8)
        Me.btnPO.Name = "btnPO"
        Me.btnPO.Size = New System.Drawing.Size(132, 23)
        Me.btnPO.TabIndex = 3
        Me.btnPO.Text = "Purchase Order"
        '
        'frmPOReport
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(691, 256)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.btnPO, Me.CrystalReportViewer1, Me.cmbIndents, Me.Label1})
        Me.Name = "frmPOReport"
        Me.Text = "frmPOReport"
        CType(Me.DsPO1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmPOReport_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim con As New OleDbConnection(constr)
        Dim cmd As New OleDbDataAdapter("select iid, iid || '-' || idesc  idesc from indents where iid in ( select  iid from quotations where accepted = 'y')", con)
        Dim ds As New DataSet()

        cmd.Fill(ds, "indents")

        cmbIndents.DataSource = ds.Tables(0)
        cmbIndents.DisplayMember = "IDESC"
        cmbIndents.ValueMember = "IID"


    End Sub

    Private Sub btnPO_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPO.Click
        OleDbDataAdapter1.SelectCommand.Parameters(0).Value = cmbIndents.SelectedValue
        DsPO1.Clear()
        OleDbDataAdapter1.Fill(DsPO1, "Indents")

        Dim r As New PO()
        r.SetDataSource(DsPO1)

        Dim robj As ReportObject
        robj = r.ReportDefinition.ReportObjects("txtMessage")
        Dim tobj As TextObject
        tobj = CType(robj, TextObject)
        tobj.Text = "Some Message"

        CrystalReportViewer1.ReportSource = r
        CrystalReportViewer1.Visible = True


    End Sub
End Class
