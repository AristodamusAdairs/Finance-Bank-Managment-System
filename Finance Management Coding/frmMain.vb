Public Class frmMain
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuNewIndent As System.Windows.Forms.MenuItem
    Friend WithEvents mnuNewQutation As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuExit As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuNewHead As System.Windows.Forms.MenuItem
    Friend WithEvents mnuUpdateHead As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuApproveIndent As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuUpdateIndent As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuIndentsReport As System.Windows.Forms.MenuItem
    Friend WithEvents mnuQuotationsReport As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPurchaseOrdere As System.Windows.Forms.MenuItem
    Friend WithEvents mnuHeadIndentsGraph As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.mnuNewIndent = New System.Windows.Forms.MenuItem()
        Me.mnuNewQutation = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.mnuExit = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.mnuNewHead = New System.Windows.Forms.MenuItem()
        Me.mnuUpdateHead = New System.Windows.Forms.MenuItem()
        Me.MenuItem4 = New System.Windows.Forms.MenuItem()
        Me.mnuApproveIndent = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.mnuUpdateIndent = New System.Windows.Forms.MenuItem()
        Me.MenuItem5 = New System.Windows.Forms.MenuItem()
        Me.mnuIndentsReport = New System.Windows.Forms.MenuItem()
        Me.mnuQuotationsReport = New System.Windows.Forms.MenuItem()
        Me.mnuPurchaseOrdere = New System.Windows.Forms.MenuItem()
        Me.mnuHeadIndentsGraph = New System.Windows.Forms.MenuItem()
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem3, Me.MenuItem4, Me.MenuItem5})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuNewIndent, Me.mnuNewQutation, Me.MenuItem2, Me.mnuExit})
        Me.MenuItem1.Text = "Transactions"
        '
        'mnuNewIndent
        '
        Me.mnuNewIndent.Index = 0
        Me.mnuNewIndent.Text = "New Indent..."
        '
        'mnuNewQutation
        '
        Me.mnuNewQutation.Index = 1
        Me.mnuNewQutation.Text = "New Qutation..."
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 2
        Me.MenuItem2.Text = "-"
        '
        'mnuExit
        '
        Me.mnuExit.Index = 3
        Me.mnuExit.Text = "Exit"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuNewHead, Me.mnuUpdateHead})
        Me.MenuItem3.Text = "Heads"
        '
        'mnuNewHead
        '
        Me.mnuNewHead.Index = 0
        Me.mnuNewHead.Text = "New..."
        '
        'mnuUpdateHead
        '
        Me.mnuUpdateHead.Index = 1
        Me.mnuUpdateHead.Text = "Update..."
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 2
        Me.MenuItem4.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuApproveIndent, Me.MenuItem6, Me.mnuUpdateIndent})
        Me.MenuItem4.Text = "Indent"
        '
        'mnuApproveIndent
        '
        Me.mnuApproveIndent.Index = 0
        Me.mnuApproveIndent.Text = "Approval.."
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 1
        Me.MenuItem6.Text = "Query..."
        '
        'mnuUpdateIndent
        '
        Me.mnuUpdateIndent.Index = 2
        Me.mnuUpdateIndent.Text = "Update..."
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 3
        Me.MenuItem5.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuIndentsReport, Me.mnuQuotationsReport, Me.mnuPurchaseOrdere, Me.mnuHeadIndentsGraph})
        Me.MenuItem5.Text = "Reports"
        '
        'mnuIndentsReport
        '
        Me.mnuIndentsReport.Index = 0
        Me.mnuIndentsReport.Text = "Indents"
        '
        'mnuQuotationsReport
        '
        Me.mnuQuotationsReport.Index = 1
        Me.mnuQuotationsReport.Text = "Quotations"
        '
        'mnuPurchaseOrdere
        '
        Me.mnuPurchaseOrdere.Index = 2
        Me.mnuPurchaseOrdere.Text = "Purchase Order"
        '
        'mnuHeadIndentsGraph
        '
        Me.mnuHeadIndentsGraph.Index = 3
        Me.mnuHeadIndentsGraph.Text = "Head-Indents Graph"
        '
        'frmMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(536, 256)
        Me.Menu = Me.MainMenu1
        Me.Name = "frmMain"
        Me.Text = "Finance Managment System"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized

    End Sub

#End Region

    Private Sub mnuNewHead_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuNewHead.Click
        Dim f As New frmNewHead()
        f.ShowDialog()
    End Sub

    Private Sub mnuNewIndent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuNewIndent.Click
        Dim f As New frmNewIndent()
        f.ShowDialog()
    End Sub

    Private Sub mnuApproveIndent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuApproveIndent.Click
        Dim f As New frmApproveIndent()
        f.ShowDialog()
    End Sub

    Private Sub mnuNewQutation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuNewQutation.Click
        Dim f As New frmNewQuotation()
        f.showdialog()
    End Sub

    Private Sub MenuItem6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem6.Click
        Dim f As New frmIndentQuery()
        f.ShowDialog()
    End Sub

    
    Private Sub mnuUpdateIndent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUpdateIndent.Click
        Dim f As New frmUpdateIndent()
        f.ShowDialog()
    End Sub

    Private Sub mnuIndentsReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuIndentsReport.Click
        Dim f As New frmIndentsReport()
        f.ShowDialog()
    End Sub

    Private Sub mnuQuotationsReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuQuotationsReport.Click
        Dim f As New frmQuotationsReport()
        f.ShowDialog()
    End Sub

    Private Sub mnuPurchaseOrdere_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPurchaseOrdere.Click
        Dim f As New frmPOReport()
        f.ShowDialog()
    End Sub

    Private Sub mnuHeadIndentsGraph_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuHeadIndentsGraph.Click
        Dim f As New frmHeadIndents()
        f.ShowDialog()
    End Sub
End Class
