Public Class frmQuotationsReport
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents OleDbDataAdapter1 As System.Data.OleDb.OleDbDataAdapter
    Friend WithEvents OleDbSelectCommand1 As System.Data.OleDb.OleDbCommand
    Friend WithEvents OleDbConnection1 As System.Data.OleDb.OleDbConnection
    Friend WithEvents DsQuotations1 As fm.dsQuotations
    Friend WithEvents CrystalReportViewer1 As CrystalDecisions.Windows.Forms.CrystalReportViewer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.OleDbDataAdapter1 = New System.Data.OleDb.OleDbDataAdapter()
        Me.OleDbSelectCommand1 = New System.Data.OleDb.OleDbCommand()
        Me.OleDbConnection1 = New System.Data.OleDb.OleDbConnection()
        Me.DsQuotations1 = New fm.dsQuotations()
        Me.CrystalReportViewer1 = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        CType(Me.DsQuotations1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'OleDbDataAdapter1
        '
        Me.OleDbDataAdapter1.SelectCommand = Me.OleDbSelectCommand1
        Me.OleDbDataAdapter1.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "QUOTATIONS", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("QID", "QID"), New System.Data.Common.DataColumnMapping("IID", "IID"), New System.Data.Common.DataColumnMapping("DQ", "DQ"), New System.Data.Common.DataColumnMapping("SUPPLIER", "SUPPLIER"), New System.Data.Common.DataColumnMapping("AMOUNT", "AMOUNT"), New System.Data.Common.DataColumnMapping("ACCEPTED", "ACCEPTED"), New System.Data.Common.DataColumnMapping("DS", "DS")})})
        '
        'OleDbSelectCommand1
        '
        Me.OleDbSelectCommand1.CommandText = "SELECT QID, IID, DQ, SUPPLIER, AMOUNT, ACCEPTED, DS FROM QUOTATIONS"
        Me.OleDbSelectCommand1.Connection = Me.OleDbConnection1
        '
        'OleDbConnection1
        '
        Me.OleDbConnection1.ConnectionString = "Provider=MSDAORA.1;Password=fm;User ID=fm"
        '
        'DsQuotations1
        '
        Me.DsQuotations1.DataSetName = "dsQuotations"
        Me.DsQuotations1.Locale = New System.Globalization.CultureInfo("en-US")
        Me.DsQuotations1.Namespace = "http://www.tempuri.org/dsQuotations.xsd"
        '
        'CrystalReportViewer1
        '
        Me.CrystalReportViewer1.ActiveViewIndex = -1
        Me.CrystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CrystalReportViewer1.Name = "CrystalReportViewer1"
        Me.CrystalReportViewer1.ReportSource = Nothing
        Me.CrystalReportViewer1.Size = New System.Drawing.Size(503, 256)
        Me.CrystalReportViewer1.TabIndex = 0
        '
        'frmQuotationsReport
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(503, 256)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.CrystalReportViewer1})
        Me.Name = "frmQuotationsReport"
        Me.Text = "QuotationsReport"
        CType(Me.DsQuotations1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmQuotationsReport_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        OleDbDataAdapter1.Fill(DsQuotations1)
        Dim r As New QuotationsReport()
        r.SetDataSource(DsQuotations1)

        CrystalReportViewer1.ReportSource = r

    End Sub
End Class
