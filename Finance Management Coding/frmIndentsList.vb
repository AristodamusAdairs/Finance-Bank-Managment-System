Imports System.Data.OleDb
Public Class frmIndentsList
    Inherits System.Windows.Forms.Form
    Private m_indentid As Integer = 0

    Public Property IndentId() As Integer
        Get
            Return m_indentid
        End Get
        Set(ByVal Value As Integer)
            m_indentid = Value
        End Set
    End Property

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lstIndents As System.Windows.Forms.ListBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.lstIndents = New System.Windows.Forms.ListBox()
        Me.SuspendLayout()
        '
        'lstIndents
        '
        Me.lstIndents.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lstIndents.Name = "lstIndents"
        Me.lstIndents.Size = New System.Drawing.Size(292, 251)
        Me.lstIndents.TabIndex = 0
        '
        'frmIndentsList
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(292, 256)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.lstIndents})
        Me.Name = "frmIndentsList"
        Me.Text = "frmIndentsList"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmIndentsList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim con As New OleDbConnection(constr)
        Dim da As New OleDbDataAdapter("select iid, iid || ',' || idesc || ',' || qty DETAILS from indents where approved = 'y'", con)
        Dim ds As New DataSet()
        da.Fill(ds, "indents")

        lstIndents.DataSource = ds.Tables("indents")
        lstIndents.DisplayMember = "DETAILS"
        lstIndents.ValueMember = "IID"

    End Sub


    Private Sub lstIndents_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstIndents.DoubleClick
        m_indentid = lstIndents.SelectedValue
        Me.Hide()
    End Sub
End Class
