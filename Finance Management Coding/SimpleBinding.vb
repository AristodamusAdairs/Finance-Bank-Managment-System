Imports System.Data.OleDb
Public Class SimpleBinding
    Inherits System.Windows.Forms.Form
    Dim ds As New DataSet()

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(30, 50)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.TabIndex = 0
        Me.TextBox1.Text = "TextBox1"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(174, 50)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.TabIndex = 1
        Me.TextBox2.Text = "TextBox2"
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(333, 50)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.TabIndex = 2
        Me.TextBox3.Text = "TextBox3"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(96, 113)
        Me.Button1.Name = "Button1"
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "Next"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(261, 115)
        Me.Button2.Name = "Button2"
        Me.Button2.TabIndex = 4
        Me.Button2.Text = "Previous"
        '
        'SimpleBinding
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(507, 256)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.Button2, Me.Button1, Me.TextBox3, Me.TextBox2, Me.TextBox1})
        Me.Name = "SimpleBinding"
        Me.Text = "SimpleBinding"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub SimpleBinding_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim con As New OleDbConnection("provider=msdaora;user id=fm;password=fm")
        Dim da As New OleDbDataAdapter("select * from heads", con)
        da.Fill(ds, "heads")

        ' bind data
        TextBox1.DataBindings.Add("text", ds, "heads.hcode")
        TextBox2.DataBindings.Add("text", ds, "heads.hdesc")
        TextBox3.DataBindings.Add("text", ds, "heads.budget")



    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.BindingContext(ds, "heads").Position = Me.BindingContext(ds, "heads").Count - 1 Then
            Beep()
        Else
            Me.BindingContext(ds, "heads").Position += 1
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If Me.BindingContext(ds, "heads").Position = 0 Then
            Beep()
        Else
            Me.BindingContext(ds, "heads").Position -= 1
        End If
    End Sub
End Class
