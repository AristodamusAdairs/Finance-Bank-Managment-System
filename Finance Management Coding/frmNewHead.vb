Imports System.Data.OleDb
Public Class frmNewHead
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtHCode As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents txtHdesc As System.Windows.Forms.TextBox
    Friend WithEvents txtBudget As System.Windows.Forms.TextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtHCode = New System.Windows.Forms.TextBox()
        Me.txtHdesc = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtBudget = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(24, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(61, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Head Code"
        '
        'txtHCode
        '
        Me.txtHCode.Location = New System.Drawing.Point(120, 8)
        Me.txtHCode.Name = "txtHCode"
        Me.txtHCode.TabIndex = 1
        Me.txtHCode.Text = ""
        '
        'txtHdesc
        '
        Me.txtHdesc.Location = New System.Drawing.Point(120, 40)
        Me.txtHdesc.Name = "txtHdesc"
        Me.txtHdesc.Size = New System.Drawing.Size(232, 20)
        Me.txtHdesc.TabIndex = 3
        Me.txtHdesc.Text = ""
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(24, 40)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(91, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Head Description"
        '
        'txtBudget
        '
        Me.txtBudget.Location = New System.Drawing.Point(120, 72)
        Me.txtBudget.Name = "txtBudget"
        Me.txtBudget.TabIndex = 5
        Me.txtBudget.Text = ""
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(24, 72)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(40, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Budget"
        '
        'btnAdd
        '
        Me.btnAdd.Location = New System.Drawing.Point(40, 120)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.TabIndex = 6
        Me.btnAdd.Text = "&Add"
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(248, 120)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.TabIndex = 7
        Me.btnCancel.Text = "&Cancel"
        '
        'frmNewHead
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(378, 158)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.btnCancel, Me.btnAdd, Me.txtBudget, Me.Label3, Me.txtHdesc, Me.Label2, Me.txtHCode, Me.Label1})
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmNewHead"
        Me.Text = "New Head"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim con As New OleDbConnection(constr)
        con.Open()

        Dim cmdstr As String

        cmdstr = "insert into heads values('" & txtHCode.Text _
           & "','" & txtHdesc.Text & "'," & txtBudget.Text & ")"
        Dim cmd As New OleDbCommand(cmdstr, con)
        cmd.ExecuteNonQuery()
        MsgBox("Added Head Successfully!", , "Status")
        con.Close()

    End Sub
End Class
