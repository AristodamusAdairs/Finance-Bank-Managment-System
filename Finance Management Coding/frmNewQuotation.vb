Imports System.Data.OleDb
Public Class frmNewQuotation
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtQID As System.Windows.Forms.TextBox
    Friend WithEvents txtIID As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnIndentList As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtQID = New System.Windows.Forms.TextBox()
        Me.txtIID = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnIndentList = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.SystemColors.Control
        Me.Label6.Enabled = False
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(32, 24)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(59, 13)
        Me.Label6.TabIndex = 28
        Me.Label6.Text = "Quation Id"
        '
        'txtQID
        '
        Me.txtQID.Location = New System.Drawing.Point(104, 24)
        Me.txtQID.Name = "txtQID"
        Me.txtQID.Size = New System.Drawing.Size(80, 20)
        Me.txtQID.TabIndex = 29
        Me.txtQID.Text = ""
        '
        'txtIID
        '
        Me.txtIID.Location = New System.Drawing.Point(360, 24)
        Me.txtIID.Name = "txtIID"
        Me.txtIID.Size = New System.Drawing.Size(80, 20)
        Me.txtIID.TabIndex = 31
        Me.txtIID.Text = ""
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.SystemColors.Control
        Me.Label1.Enabled = False
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(288, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 13)
        Me.Label1.TabIndex = 30
        Me.Label1.Text = "Indent  Id"
        '
        'btnIndentList
        '
        Me.btnIndentList.Location = New System.Drawing.Point(448, 24)
        Me.btnIndentList.Name = "btnIndentList"
        Me.btnIndentList.Size = New System.Drawing.Size(24, 16)
        Me.btnIndentList.TabIndex = 32
        Me.btnIndentList.Text = "..."
        '
        'frmNewQuotation
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(536, 256)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.btnIndentList, Me.txtIID, Me.Label1, Me.txtQID, Me.Label6})
        Me.Name = "frmNewQuotation"
        Me.Text = "frmNewQuotation"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmNewQutation_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtQID.Text = GetQuotationID()
    End Sub

    Function GetQuotationID() As Integer
        Dim con As New OleDbConnection(constr)
        con.Open()
        Dim cmd As New OleDbCommand("select  nvl(max(qid),0)  + 1 from quotations", con)
        Dim id As Integer

        id = cmd.ExecuteScalar
        con.Close()
        Return id


    End Function

    Private Sub btnIndentList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIndentList.Click
        Dim f As New frmIndentsList()
        f.ShowDialog()

        If f.IndentId <> 0 Then
            txtIID.Text = f.IndentId
        End If

    End Sub
End Class
