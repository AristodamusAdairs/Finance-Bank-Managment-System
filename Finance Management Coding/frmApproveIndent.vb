Imports System.Data.OleDb
Public Class frmApproveIndent
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents txtPlacedBy As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtQty As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtIDesc As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtIID As System.Windows.Forms.TextBox
    Friend WithEvents txtHcode As System.Windows.Forms.TextBox
    Friend WithEvents txtDI As System.Windows.Forms.TextBox
    Friend WithEvents dtpDA As System.Windows.Forms.DateTimePicker
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.txtPlacedBy = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtQty = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtIDesc = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtHcode = New System.Windows.Forms.TextBox()
        Me.txtDI = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.txtIID = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.dtpDA = New System.Windows.Forms.DateTimePicker()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button2
        '
        Me.Button2.CausesValidation = False
        Me.Button2.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Button2.Location = New System.Drawing.Point(384, 232)
        Me.Button2.Name = "Button2"
        Me.Button2.TabIndex = 23
        Me.Button2.Text = "&Cancel"
        '
        'btnAdd
        '
        Me.btnAdd.Location = New System.Drawing.Point(192, 232)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.TabIndex = 22
        Me.btnAdd.Text = "&Approve"
        '
        'txtPlacedBy
        '
        Me.txtPlacedBy.Enabled = False
        Me.txtPlacedBy.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPlacedBy.Location = New System.Drawing.Point(460, 168)
        Me.txtPlacedBy.Name = "txtPlacedBy"
        Me.txtPlacedBy.Size = New System.Drawing.Size(88, 20)
        Me.txtPlacedBy.TabIndex = 21
        Me.txtPlacedBy.Text = ""
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Silver
        Me.Label5.Enabled = False
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(324, 168)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(93, 13)
        Me.Label5.TabIndex = 20
        Me.Label5.Text = "Indent Placed By"
        '
        'txtQty
        '
        Me.txtQty.Enabled = False
        Me.txtQty.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtQty.Location = New System.Drawing.Point(164, 160)
        Me.txtQty.Name = "txtQty"
        Me.txtQty.Size = New System.Drawing.Size(88, 20)
        Me.txtQty.TabIndex = 19
        Me.txtQty.Text = ""
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Silver
        Me.Label4.Enabled = False
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(100, 160)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(48, 13)
        Me.Label4.TabIndex = 18
        Me.Label4.Text = "Quantity"
        '
        'txtIDesc
        '
        Me.txtIDesc.Enabled = False
        Me.txtIDesc.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIDesc.Location = New System.Drawing.Point(164, 120)
        Me.txtIDesc.Name = "txtIDesc"
        Me.txtIDesc.Size = New System.Drawing.Size(384, 20)
        Me.txtIDesc.TabIndex = 17
        Me.txtIDesc.Text = ""
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Silver
        Me.Label3.Enabled = False
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(100, 120)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(63, 13)
        Me.Label3.TabIndex = 16
        Me.Label3.Text = "Description"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Silver
        Me.Label2.Enabled = False
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(288, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(78, 13)
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "Date of Indent"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Silver
        Me.Label1.Enabled = False
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(100, 76)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 13)
        Me.Label1.TabIndex = 12
        Me.Label1.Text = "Head"
        '
        'txtHcode
        '
        Me.txtHcode.Enabled = False
        Me.txtHcode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHcode.Location = New System.Drawing.Point(164, 72)
        Me.txtHcode.Name = "txtHcode"
        Me.txtHcode.Size = New System.Drawing.Size(88, 20)
        Me.txtHcode.TabIndex = 24
        Me.txtHcode.Text = ""
        '
        'txtDI
        '
        Me.txtDI.Enabled = False
        Me.txtDI.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDI.Location = New System.Drawing.Point(456, 72)
        Me.txtDI.Name = "txtDI"
        Me.txtDI.Size = New System.Drawing.Size(88, 20)
        Me.txtDI.TabIndex = 25
        Me.txtDI.Text = ""
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Silver
        Me.Panel1.Controls.AddRange(New System.Windows.Forms.Control() {Me.Label2})
        Me.Panel1.Enabled = False
        Me.Panel1.Location = New System.Drawing.Point(72, 48)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(528, 176)
        Me.Panel1.TabIndex = 26
        '
        'txtIID
        '
        Me.txtIID.Location = New System.Drawing.Point(176, 16)
        Me.txtIID.Name = "txtIID"
        Me.txtIID.Size = New System.Drawing.Size(88, 20)
        Me.txtIID.TabIndex = 28
        Me.txtIID.Text = ""
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.SystemColors.Control
        Me.Label6.Enabled = False
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(72, 16)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(85, 13)
        Me.Label6.TabIndex = 27
        Me.Label6.Text = "Indent Number "
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.SystemColors.Control
        Me.Label7.Enabled = False
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(320, 16)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(92, 13)
        Me.Label7.TabIndex = 29
        Me.Label7.Text = "Date of Approval"
        '
        'dtpDA
        '
        Me.dtpDA.CustomFormat = "dd-MMM-yy"
        Me.dtpDA.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpDA.Location = New System.Drawing.Point(464, 16)
        Me.dtpDA.Name = "dtpDA"
        Me.dtpDA.Size = New System.Drawing.Size(136, 20)
        Me.dtpDA.TabIndex = 30
        '
        'frmApproveIndent
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(632, 268)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.dtpDA, Me.Label7, Me.txtIID, Me.Label6, Me.txtDI, Me.txtHcode, Me.Button2, Me.btnAdd, Me.txtPlacedBy, Me.Label5, Me.txtQty, Me.Label4, Me.txtIDesc, Me.Label3, Me.Label1, Me.Panel1})
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmApproveIndent"
        Me.Text = "Approve Indent"
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub txtIID_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtIID.Validating
        ' get details of indent
        Dim con As New OleDbConnection(constr)
        con.Open()
        Dim cmd As New OleDbCommand("select * from indents where  approved = 'n' and iid = " & txtIID.Text, con)
        Dim dr As OleDbDataReader

        dr = cmd.ExecuteReader

        If dr.Read() Then
            ' copy data to controls
            txtHcode.Text = dr.Item("hcode")
            txtIDesc.Text = dr.Item("idesc")
            txtDI.Text = dr.Item("di")
            txtQty.Text = dr.Item("qty")
            txtPlacedBy.Text = dr.Item("placedby")

        Else
            MsgBox("Sorry! Invalid Indent Number.Please try again!", , "Error")
            e.Cancel = True
        End If

        dr.Close()
        con.Close()

    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ' update table
        Dim con As New OleDbConnection(constr)
        con.Open()

        Dim cmdstr As String
        cmdstr = "update indents  set  approved = 'y', da = '" & dtpDA.Text & "'  where  iid = " & txtIID.Text

        Dim cmd As New OleDbCommand(cmdstr, con)
        Try
            cmd.ExecuteNonQuery()
            MsgBox("Indent is approved!", , "Status")
            ' clear textboxes
            Dim c As System.Windows.Forms.Control

            For Each c In Me.Controls
                If TypeOf c Is System.Windows.Forms.TextBox Then
                    c.Text = ""
                End If
            Next
            dtpDA.Value = Now()
            txtIID.Focus()
        Catch ex As Exception
            MsgBox(ex.Message, , "Error")
        Finally
            con.Close()
        End Try
    End Sub
End Class
